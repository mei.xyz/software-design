import FileHandler from "../classes/fileHandler";
import { Questionnaire } from "./questionnaire/questionnaire";
import { QuestionnaireAdapter } from "./questionnaire/questionnaireAdapter";
import { Main } from "../index";
const { prompt } = require("enquirer");
const { Form } = require("enquirer");
const { Confirm } = require("enquirer");
const { Select } = require("enquirer");
const { Input } = require("enquirer");

export default class User {
    public firstname: string = "";
    public username: string = "";
    private _password: string = "";
    public takenSurveys: string[] = [];
    public ownSurveys: string[] = [];
    private static _singleton: User = new User();

    private constructor() {
        if (User._singleton) throw new Error("Use User.getInstance() instead new User()");
        User._singleton = this;
    }

    public static getInstance(): User {
        return User._singleton;
    }

    public async register(): Promise<void> {
        const prompt = new Form({
            type: "form",
            message: "Please provide the following information:",
            choices: [
                { name: "firstname", message: "First Name" },
                { name: "username", message: "Username" },
                { name: "password", message: "Password" },
            ],
        });

        await prompt
            .run()
            .then(async (value: { [k: string]: any }) => {
                let checkIfAlphanumeric: RegExp = /^[a-zA-Z0-9]+$/;

                if (value.username == "" || value.password == "" || value.firstname == "") {
                    console.log("\nNone of the fields can be empty.\n");
                    await this.register();
                } else {
                    let allUsers: any[] = FileHandler.readArrayFile("../data/user.json");
                    let checkIfExists: boolean = allUsers.some((data) => data.username === value.username);

                    if (checkIfExists) {
                        console.log("\nSorry, this username already exists!\n");
                        await this.register();
                    } else if (checkIfAlphanumeric.test(value.username) == false) {
                        console.log("\nYour username can only consist of the following signs:");
                        console.log("A-Z, a-z, 0-9\n");
                        await this.register();
                    } else {
                        value.takenSurveys = [];
                        value.ownSurveys = [];
                        FileHandler.addToJSONArray("../data/user.json", value);
                        console.log("\nYou're registered!\n");
                    }
                }
            })
            .catch(console.error);
    }

    public async login(): Promise<void> {
        const question = [
            { type: "input", name: "username", message: "Username" },
            { type: "password", name: "password", message: "Password" },
        ];

        const userArray = FileHandler.readArrayFile("../data/user.json");

        await prompt(question)
            .then((answer: { [key: string]: number }) => {
                for (var i = 0; i < userArray.length; i++) {
                    if (userArray[i].username == answer["username"]) {
                        if (userArray[i].password == answer["password"]) {
                            this.username = userArray[i].username; // this means someone is logged in
                            this.firstname = userArray[i].firstname;
                            this.takenSurveys = userArray[i].takenSurveys;
                            this.ownSurveys = userArray[i].ownSurveys;
                            console.log("\nLogged in. Welcome, " + userArray[i].firstname + "!\n");
                        } else {
                            console.log("\nUsername and password don't match. Please try again.\n");
                            break;
                        }
                    }
                    if (i == userArray.length - 1 && this.username == "" && userArray[i].username != answer["username"]) {
                        console.log("\nUsername can't be found. Please try again.\n");
                    }
                }
            })
            .catch(console.error);
    }

    public async logout(): Promise<void> {
        this.username = "";
        this.firstname = "";
        this.takenSurveys = [];
        this.ownSurveys = [];
    }

    public async createQuestionnaire(): Promise<void> {
        let newQuestionnaire;
        console.log("\nCreating a new questionnaire...\n");

        const prompt = new Input({
            message: "Title of your questionnaire:",
            initial: "title",
        });

        await prompt
            .run()
            .then(async (title: string) => {
                let survey: Questionnaire = await FileHandler.searchSpecificQuestionnaire(title);
                if (survey.title == title) {
                    console.log("\nThis title is already taken, please change it up a bit.");
                    await this.createQuestionnaire();
                } else {
                    console.log("\nPlease choose a timespan in which your questionnaire will be available.");
                    console.log("If you want it to be open today, choose yesterday's date.");
                    let startDate: string = await this.helperCreateQuestionnaire(
                        "Start of your questionnaire:"
                    );
                    let endDate: string = await this.helperCreateQuestionnaire("End of your questionnaire:");

                    newQuestionnaire = new Questionnaire(title, this.username, startDate, endDate);
                    console.log("\n The questionnaire must contain at least 5 questions.");

                    // adding questions and answers if necessary
                    let addMore: boolean = true;
                    let counter: number = 0;
                    while (addMore) {
                        let type: string = await newQuestionnaire.pickQuestionType();
                        console.log("\n");
                        newQuestionnaire.questions.push(await newQuestionnaire.addQuestion(type));

                        counter++;
                        if (counter >= 5) {
                            console.log("\n");
                            const promp = new Confirm({
                                message: "Add another question?",
                            });

                            await promp.run().then((answer: boolean) => {
                                if (answer) addMore = true;
                                else addMore = false;
                            });
                        }
                    }
                    console.log("\nSaving...");
                    FileHandler.addToJSONArray("../data/questionnaire.json", newQuestionnaire);
                    console.log("All done!");

                    //saving survey title in user object
                    let currentUser: object = await FileHandler.searchUser(this.username);
                    Object.values(currentUser)[4].push(title);
                    await FileHandler.updateUser(this.username, currentUser);
                    this.ownSurveys.push(title);
                }
            })
            .catch(console.error);
    }

    public async helperCreateQuestionnaire(message: string): Promise<string> {
        let checkIfDate: RegExp = /(202)\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/;

        const prompt = new Input({
            message: message,
            initial: "YYYY-MM-DD",
        });

        return prompt.run().then(async (date: string) => {
            if (checkIfDate.test(date)) return date;
            else {
                console.log("Please enter a proper date.");
                return await this.helperCreateQuestionnaire(message);
            }
        });
    }

    public async selectQuestionnaire(listOfTitles: string[], page: number): Promise<void> {
        const prompt = new Select({
            name: "type",
            message: "Select a questionnaire:",
            choices: listOfTitles,
        });

        await prompt
            .run()
            .then(async (answer: string) => {
                if (answer == "> next page") {
                    this.helperSelectQuestionnaire(page + 1);
                } else if (answer == "< previous page") this.helperSelectQuestionnaire(page - 1);
                else {
                    let splitted = answer.split(" [>", 2); // split string bc only title is needed
                    let survey: Questionnaire = await FileHandler.searchSpecificQuestionnaire(splitted[0]);
                    await this.playQuestionnaire(survey);
                }
            })
            .catch(console.error);
    }

    public async helperSelectQuestionnaire(currentPage: number, search?: string): Promise<void> {
        let list: Questionnaire[] = FileHandler.readObjectFile("../data/questionnaire.json");
        let listForSearch: Questionnaire[] = [];
        let listExists: boolean = true;
        let menu = new Main();

        let totalPages: number = Math.ceil(list.length / 5);
        let page: number = currentPage;
        let listOfTitles: string[] = [];
        let i = 0 + page * 10; // 10 bc 10 surveys per page
        let counter = 0;
        let next: string = "> next page";
        let previous: string = "< previous page";

        if (search != undefined) {
            for (let i = 0; i < list.length; i++)
                if (list[i].title.includes(search)) listForSearch.push(list[i]);
            list = listForSearch;

            if (list.length == 0) {
                console.log("\nSorry, I couldn't find anything.\n");
                listExists = false;
                await this.whatToDoInstead(true);
            } else console.log("\nFound something!\n");
        }

        if (listExists) {
            for (i; i < list.length; i++) {
                if (menu.checkDate(list[i].availableFrom, list[i].availableUntil) == "ok")
                    listOfTitles[counter] = list[i].title + " [> participants: " + list[i].amountTaken + "]";
                if (menu.checkDate(list[i].availableFrom, list[i].availableUntil) == "not yet")
                    listOfTitles[counter] =
                        list[i].title +
                        " [> blocked | opens: " +
                        list[i].availableFrom +
                        "]";
                if (menu.checkDate(list[i].availableFrom, list[i].availableUntil) == "closed")
                    listOfTitles[counter] =
                        list[i].title +
                        " [> participants: " +
                        list[i].amountTaken +
                        ", blocked | closed: " +
                        list[i].availableUntil +
                        "]";
                counter++;
                if (page > 0) listOfTitles[counter] = previous;
                if (counter == 10 && list[i+1]) {
                    if (page > 0 && page < totalPages - 1) {
                        listOfTitles[counter + 1] = next;
                    } else if (page < totalPages - 1) listOfTitles[counter] = next;
                    break;
                }
            }
            await this.selectQuestionnaire(listOfTitles, page);
        }
    }

    public async searchQuestionnaire(): Promise<void> {
        const prompt = new Input({
            message: "What are you looking for?",
        });

        await prompt
            .run()
            .then(async (answer: string) => {
                await this.helperSelectQuestionnaire(0, answer);
            })
            .catch(console.log);
    }

    public async playQuestionnaire(questionnaire: Questionnaire): Promise<void> {
        let questionnaireAdpater = new QuestionnaireAdapter(questionnaire);
        await questionnaireAdpater.helperPlayQuestionnaire();
    }

    public async viewStatistics(): Promise<void> {
        if (this.ownSurveys.length == 0 || this.username == "") {
            console.log("\nAmount of questionnaires you have taken: " + this.takenSurveys.length);
            for (let i = 0; i < this.takenSurveys.length; i++) {
                console.log(this.takenSurveys[i]);
            }
        } else {
            const prompt = new Select({
                message: "Select a questionnaire:",
                choices: ["Questionnaires you have taken", "Questionnaires you created"],
            });

            await prompt.run().then(async (answer: string) => {
                if (answer == "Questionnaires you have taken") {
                    console.log("\nAmount of questionnaires you have taken: " + this.takenSurveys.length);

                    for (let i = 0; i < this.takenSurveys.length; i++) {
                        console.log("> " + this.takenSurveys[i]);
                    }
                } else {
                    let list: string[] = [];
                    console.log("\nAmount of questionnaires you created: " + this.ownSurveys.length);

                    for (let i = 0; i < this.ownSurveys.length; i++) {
                        let survey: Questionnaire = await FileHandler.searchSpecificQuestionnaire(
                            this.ownSurveys[i]
                        );
                        list[i] = survey.title;
                    }
                    const prompt = new Select({
                        name: "type",
                        message: "Select a questionnaire:",
                        choices: list,
                    });

                    await prompt.run().then(async (answer: string) => {
                        let survey: Questionnaire = await FileHandler.searchSpecificQuestionnaire(answer);
                        if (survey.amountTaken != 0) {
                            let questionnaireAdpater = new QuestionnaireAdapter(survey);
                            await questionnaireAdpater.viewStatistics();
                        } else {
                            console.log("\nThis questionnaire hasn't been taken yet.");
                        }
                    });
                }
            });
        }
    }

    public async whatToDoInstead(search: boolean): Promise<void> {
        let menu = new Main();

        if (search == false) {
            const prompt = new Select({
                name: "type",
                message: "What do you want to do instead?",
                choices: ["Back to menu", "Select a different questionnaire"],
            });
            await prompt.run().then(async (answer: string) => {
                if (answer == "Back to menu") {
                    menu.showMenu();
                } else await this.helperSelectQuestionnaire(0);
            });
        } else {
            const prompt = new Select({
                name: "type",
                message: "What do you want to do instead?",
                choices: ["Back to menu", "Do another search"],
            });
            await prompt.run().then(async (answer: string) => {
                if (answer == "Back to menu") {
                    menu.showMenu();
                } else await this.searchQuestionnaire();
            });
        }
    }
}
