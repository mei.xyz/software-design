import { Question } from "./question";
const { Confirm } = require("enquirer");
const { Select } = require("enquirer");
const { v4: uuidv4 } = require("uuid");

export class Questionnaire {
    public title: string = "";
    public author: string = "";
    public uuid: string = "";
    public availableFrom: string;
    public availableUntil: string;
    public amountTaken: number;
    public questions: Question[] = [];

    constructor(_title: string, _author: string, availableFrom: string, availableUntil: string) {
        this.title = _title;
        this.author = _author;
        this.uuid = uuidv4();
        this.availableFrom = availableFrom;
        this.availableUntil = availableUntil;
        this.amountTaken = 0;
    }

    public async addQuestion(type: string): Promise<Question> {
        const { Input } = require("enquirer");
        const prompt = new Input({
            message: "Title of your question:",
            initial: "title",
        });

        return prompt
            .run()
            .then(async (answer: string): Promise<Question> => {
                let question = new Question(answer, type);

                if (type == "Multiple choice") {
                    let addMore: boolean = true;
                    let counter: number = 0;
                    while (addMore && counter < 10) {
                        question.answers.push(await question.addNewAnswer());
                        question.answerStats.push(0);

                        counter++;

                        if (counter >= 2 && counter < 10) {
                            const promp = new Confirm({
                                message: "Add another answer?",
                            });

                            await promp.run().then((answer: boolean) => {
                                if (answer) addMore = true;
                                else addMore = false;
                            });
                        }
                    }
                } else if (type == "True or false") {
                    question.answers = [true, false];
                    question.answerStats = [0, 0];
                } else if (type == "Rating (0-4)") {
                    question.answers = [0, 1, 2, 3, 4];
                    question.answerStats = [0, 0, 0, 0, 0];
                }
                return question;
            })
            .catch(console.error);
    }

    public async pickQuestionType(): Promise<string> {
        const prompt = new Select({
            name: "type",
            message: "Pick a question type",
            choices: ["Multiple choice", "True or false", "Rating (0-4)"],
        });
        return prompt.run().catch(console.error);
    }
}
