import { Question } from "./question";
import { Questionnaire } from "./questionnaire";
import { Main } from "../../index";
import User from "../user";
import FileHandler from "../../classes/fileHandler";
const { Confirm } = require("enquirer");

export class QuestionnaireAdapter {
    private _questionnaire: Questionnaire;
    private _user: User = User.getInstance();

    constructor(questionnaire_in: Questionnaire) {
        this._questionnaire = questionnaire_in;
    }

    public async helperPlayQuestionnaire(): Promise<void> {
        let checkIfTaken: number = this._user.takenSurveys.indexOf(this._questionnaire.title);
        let menu = new Main();

        if (menu.checkDate(this._questionnaire.availableFrom, this._questionnaire.availableUntil) == "ok") {
            if (this._questionnaire.author == this._user.username) {
                console.log("\nYou can't participate in your own questionnaire, sorry!\n");
                await this._user.helperSelectQuestionnaire(0);
            } else {
                console.log(
                    "\nStart: " + this._questionnaire.availableFrom + ", End: " +
                        this._questionnaire.availableUntil + "\n"
                );
                if (checkIfTaken == -1) {
                    const prompt = new Confirm({
                        message: "Do you want to take this survey?",
                    });

                    await prompt.run().then(async (answer: boolean) => {
                        if (answer) await this.playQuestionnaire();
                        else await this._user.whatToDoInstead(false);
                    });
                } else {
                    console.log("Sorry, you've already taken this survey!\n");
                    await this._user.whatToDoInstead(false);
                }
            }
        } else {
            console.log("\nSorry, this questionnaire is currently not available.");
            console.log("\nStart: " + this._questionnaire.availableFrom);
            console.log("End: " + this._questionnaire.availableUntil + "\n");
            await this._user.whatToDoInstead(false);
        }
    }

    public async playQuestionnaire(): Promise<void> {
        console.log("\nSurvey: " + this._questionnaire.title);
        let arr: Array<string> = [];
        let surveyQuestion;
        for (let i = 0; i < this._questionnaire.questions.length; i++) {
            surveyQuestion = new Question(
                this._questionnaire.questions[i].title,
                this._questionnaire.questions[i].type
            );

            arr = []; //cashe the array bc select-prompt changes given array
            for (let ans = 0; ans < this._questionnaire.questions[i].answers.length; ans++)
                arr[ans] = this._questionnaire.questions[i].answers[ans];

            let chosenAnswer: number = await surveyQuestion.handleQuestionType(
                this._questionnaire.questions[i].answers
            );
            this._questionnaire.questions[i].answers = arr;
            this._questionnaire.questions[i].answerStats[chosenAnswer] += 1;
        }
        this._questionnaire.amountTaken += 1;
        await FileHandler.updateObject(this._questionnaire.title, this._questionnaire);

        if (this._user.username != "") {
            //saving survey title in user object
            let currentUser: object = await FileHandler.searchUser(this._user.username);
            Object.values(currentUser)[3].push(this._questionnaire.title);
            await FileHandler.updateUser(this._user.username, currentUser);
        }
        this._user.takenSurveys.push(this._questionnaire.title);
        console.log("\nThank you for participating!");
        let menu = new Main();
        menu.leaveProgram(true);
    }

    public async viewStatistics(): Promise<void> {
        console.log(
            "\nSurvey: " + this._questionnaire.title + " (amount taken: " +
                this._questionnaire.amountTaken + ")"
        );
        console.log(
            "Start: " + this._questionnaire.availableFrom + ", End: " +
                this._questionnaire.availableUntil
        );

        for (let i = 0; i < this._questionnaire.questions.length; i++) {
            console.log("\n> " + this._questionnaire.questions[i].title);
            for (let ans = 0; ans < this._questionnaire.questions[i].answers.length; ans++) {
                console.log(
                    this._questionnaire.questions[i].answers[ans] +
                        " chosen: " + this._questionnaire.questions[i].answerStats[ans]
                );
            }
        }
    }
}
