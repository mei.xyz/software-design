import fs from "fs";
import path from "path";
import User from "./user";
import { Questionnaire } from "./questionnaire/questionnaire";

export class FileHandler {
    private static _instance: FileHandler = new FileHandler();

    private constructor() {
        if (FileHandler._instance) throw new Error("Use FileHandler.getInstance() instead new FileHandler()");
        FileHandler._instance = this;
    }

    public static getInstance(): FileHandler {
        return FileHandler._instance;
    }

    private readFile(pathToFile: string): any {
        let jsonRaw = fs.readFileSync(path.resolve(__dirname, "../" + pathToFile));
        let json: any = JSON.parse(jsonRaw.toString());
        return json;
    }

    public readArrayFile(pathToFile: string): Array<any> {
        return this.readFile(pathToFile);
    }

    public readObjectFile(pathToFile: string): any {
        return this.readFile(pathToFile);
    }

    public writeFile(pathToFile: string, dataToWrite: any): void {
        fs.writeFileSync(path.resolve(__dirname, "../" + pathToFile), JSON.stringify(dataToWrite));
    }

    public addToJSONArray(pathToFile: string, dataToWrite: any): void {
        let jsonFile: any = this.readFile(pathToFile);
        jsonFile.push(dataToWrite);
        fs.writeFileSync(path.resolve(__dirname, "../" + pathToFile), JSON.stringify(jsonFile));
    }

    public async searchSpecificQuestionnaire(search: string): Promise<Questionnaire> {
        let list: Array<Questionnaire> = this.readArrayFile("../data/questionnaire.json");
        for (let i = 0; i < list.length; i++) if (list[i].title == search) return list[i];
        return new Questionnaire("", "", "", "");
    }

    public async searchUser(search: string): Promise<object> {
        let list: any[] = this.readArrayFile("../data/user.json");
        for (let i = 0; i < list.length; i++) if (list[i].username == search) return list[i];
        return User.getInstance();
    }

    public async updateObject(title: string, updatedobject: object): Promise<void> {
        let jsonFile: any = this.readArrayFile("../data/questionnaire.json");
        for (let i = 0; i < jsonFile.length; i++) if (jsonFile[i].title == title) jsonFile[i] = updatedobject;
        fs.writeFileSync(path.resolve(__dirname, "../../data/questionnaire.json"), JSON.stringify(jsonFile));
    }

    public async updateUser(name: string, updatedobject: object): Promise<void> {
        let jsonFile: any = this.readArrayFile("../data/user.json");
        for (let i = 0; i < jsonFile.length; i++)
            if (jsonFile[i].username == name) jsonFile[i] = updatedobject;
        fs.writeFileSync(path.resolve(__dirname, "../../data/user.json"), JSON.stringify(jsonFile));
    }
}

export default FileHandler.getInstance();
