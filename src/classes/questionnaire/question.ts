const { Select } = require("enquirer");
const { Confirm } = require("enquirer");
const { Survey } = require("enquirer");

export class Question {
    public title!: string;
    public type!: string;
    public answers: any[] = [];
    public answerStats: number[] = [];

    constructor(_title: string, _type: string) {
        this.title = _title;
        this.type = _type;
        this.answers = [];
        this.answerStats = [];
    }

    public async addNewAnswer(): Promise<string> {
        const { Input } = require("enquirer");
        const prompt = new Input({
            message: "Answer:",
            initial: "answer",
        });

        return prompt.run().catch(console.error);
    }

    public async handleQuestionType(answerArray: any[]): Promise<any> {
        let arr: Array<any> = [];
        for (let i = 0; i < answerArray.length; i++) arr[i] = answerArray[i];

        if (this.type == "Multiple choice") {
            const prompt = new Select({
                message: this.title,
                choices: answerArray,
            });

            return prompt
                .run()
                .then(async (answer: any) => {
                    let index: number;
                    return (index = arr.indexOf(answer));
                })
                .catch(console.error);
        }
        if (this.type == "True or false") {
            const promp = new Confirm({
                message: this.title,
            });

            return promp
                .run()
                .then(async (answer: boolean) => {
                    let index: number;
                    return (index = arr.indexOf(answer));
                })
                .catch(console.error);
        }
        if (this.type == "Rating (0-4)") {
            const prompt = new Survey({
                message: "Please rate:",
                scale: [
                    { name: 0, message: "Strongly Disagree" },
                    { name: 1, message: "Disagree" },
                    { name: 2, message: "Neutral" },
                    { name: 3, message: "Agree, Good" },
                    { name: 4, message: "Strongly Agree" }
                ],
                choices: [
                    { message: this.title }
                ]
            });

            return prompt
                .run()
                .then(async (answer: object) => {
                    let index: number;
                    return (index = arr.indexOf(Object.values(answer)[0]));
                })
                .catch(console.error);
        }
    }
}
