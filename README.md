# Software Design: Questionnaires
***
Hochschule Furtwangen, Fakultät Digitale Medien, Medieninformatik 4
Meike Nauber, Matrikelnummer: 263279


## Allgemeines
***
1. Repository clonen
```
$ git clone https://gitlab.com/mei.xyz/software-design.git
```
2. Dependencies installieren
```
$ npm install
```
3. Terminal im Ordner öffnen und starten mit 
```
$ npm run start
```

Test kann ausgeführt werden mit
```
$ npm run test
```

## Abschluss
***
Vielen Dank für's Testen. Ich hoffe, es klappt alles! 
Bestehende Accounts können in der user.json abgerufen werden. 
Beispielaccount: 
```
username: nineteen
password: banana
```
