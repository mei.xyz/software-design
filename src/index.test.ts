import { Main } from "./index";

describe("This is a simple test", () => {
    test("Check if current date is between two given dates", () => {
        let menu = new Main;
        expect(menu.checkDate("2021-07-12", "2021-08-21")).toBe("ok");
    });
});