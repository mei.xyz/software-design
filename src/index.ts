import ConsoleHandler from "../src/classes/consoleHandler";
import User from "../src/classes/user";
const { prompt } = require("enquirer");

export class Main {
    private _user: User = User.getInstance();

    public showProgramStatus(): void {
        console.log("\nWelcome to Questionnairio!\n");
    }

    public showMenu(): void { 
        if (this._user.username == "") { // if unregistered or not logged in
            prompt({
                type: "select",
                name: "menu",
                message: "Choose",
                choices: [
                    "Register",
                    "Login",
                    "Select questionnaire",
                    "Search for questionnaire",
                    "View statistics",
                    "End program",
                ],
            })
                .then((answer: object) => this.handleAnswer(answer))
                .catch(console.error);
        } else { // after login
            prompt({
                type: "select",
                name: "menu",
                message: "Choose",
                choices: [
                    "Select questionnaire",
                    "Search for questionnaire",
                    "Create questionnaire",
                    "View statistics",
                    "Logout",
                    "End program",
                ],
            })
                .then((answer: object) => this.handleAnswer(answer))
                .catch(console.error);
        }
    }

    public async handleAnswer(answer: object) {
        switch (Object.values(answer)[0]) {
            case "Register":
                this._user.register().then(() => this.leaveProgram(false));
                break;
            case "Login":
                this._user.login().then(() => this.leaveProgram(false));
                break;
            case "Select questionnaire":
                this._user.helperSelectQuestionnaire(0);
                break;
            case "Search for questionnaire":
                this._user.searchQuestionnaire();
                break;
            case "Create questionnaire":
                this._user.createQuestionnaire().then(() => this.leaveProgram(true));
                break;
            case "View statistics":
                this._user.viewStatistics().then(() => this.leaveProgram(true));
                break;
            case "Logout":
                this._user.logout().then(() => this.leaveProgram(false));
                break;
            case "End program":
                ConsoleHandler.closeConsole();
                break;
        }
    }

    public async leaveProgram(ask: boolean): Promise<void> {
        if (ask) {
            console.log("\n");
            prompt({
                type: "confirm",
                name: "question",
                message: "Back to menu?",
            })
                .then((answer: object) => {
                    if (Object.values(answer)[0] == true) this.showMenu();
                    else console.log("\nThank you, have a good day!\n");
                })
                .catch(console.error);
        } else {
            this.showMenu();
        }
    }

    public checkDate(startDate: string, endDate: string): string {
        let currentDate = new Date().toJSON().slice(0, 10);
        let from = new Date(startDate);
        let to = new Date(endDate);
        let check = new Date(currentDate);

        if (check > from && check < to) return "ok";
        if (check < from) return "not yet";
        else return "closed";
    }
}

let main: Main = new Main();
main.showProgramStatus();
main.showMenu();
