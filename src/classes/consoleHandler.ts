import * as readline from "readline";

class ConsoleHandler {
    private static _singleton: ConsoleHandler = new ConsoleHandler();

    private _line: readline.ReadLine = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    constructor() {
        if (ConsoleHandler._singleton)
            throw new Error("Use ConsoleHandler.getInstance() instead new ConsoleHandler()");
        ConsoleHandler._singleton = this;
    }

    public static getInstance(): ConsoleHandler {
        return ConsoleHandler._singleton;
    }

    public closeConsole() {
        this._line.write("\nThank you, have a good day!\n");
        this._line.close();
    }
}

export default ConsoleHandler.getInstance();
